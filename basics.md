## Basics

Mount one component and solder it in place, that way it;s easier to keep track of your work and keep the components close to the board, start with the lower ones.

For the micro, solder the bottom part without the micro on top, that way you do not overeat the component (and risk on breaking it).

Mount the components on the same side as the outline on the board:


![Alt text](img/Picture1.png?raw=true "Mount components on this side.")



The side with the soldering spots is there the legs of the components are supposed to come out.


## Polarity + and -

A  **polarized**  component, a part _with_ polarity can only be connected to a circuit in one direction.

**Warning: if you connect the component wrong, you could end up with a dead component, smoke, sparks, burns. NEVER reuse a dead component.**

## Diode and LED Polarity

[Diodes] only allow current to flow in one direction, and they&#39;re _always_ polarized.



![Alt text](img/Picture2.png?raw=true "polarity")




![Alt text](img/Picture4.png?raw=true "led")



_The diode circuit symbol, with the anode and cathode marked._

Usually the diode will have a  **line near the cathode pin** , which matches the vertical line in the diode circuit symbol.



![Alt text](img/Picture5.png?raw=true "diode.")



Below are a few examples of diodes. The top diode, a  [1N4001]) rectifier, has a grey ring near the cathode. Below that, a  [1N4148] signal diode uses a black ring to mark the cathode. At the bottom are a couple surface mount diodes, each of which use a line to mark which pin is the cathode.




![Alt text](img/Picture3.png?raw=true "diodes")



### LEDs

The  **longer leg ** indicates the positive, anode pin.

Or, if someone&#39;s trimmed the legs, try finding the flat edge on the LED&#39;s outer casing. The pin nearest the  **flat edge**  will be the negative, cathode pin.



## Integrated Circuit Polarity

Integrated circuits (ICs) might have eight pins or eighty pins, and each pin on an IC has a unique function and position. It&#39;s very important to keep polarity straight with ICs. There&#39;s a good chance they&#39;ll smoke, melt, and be ruined if connected incorrectly.

Through-hole ICs usually come in a dual-inline package (DIP) – two rows of pins, each spaced by 0.1&quot; wide enough to straddle the center of a breadboard. DIP ICs usually have a  **notch**  to indicate which of the many pins is the first. If not a notch, the IC might have an etched  **dot**  in the casing near pin 1.


_An IC with both a dot and a notch to indicate polarity. Sometimes you get both, sometimes you only get one or the other._

For all IC packages, pin numbers increase sequentially as you move counter-clockwise away from pin 1.


Surface-mount ICs might come in QFN, SOIC, SSOP, or a number of other form-factors. These ICs will usually have a  **dot**  near pin 1.




![Alt text](img/Picture6.png?raw=true "IC")





_An _ [_ATmega32U4_]_ in a TQFP package, next to the datasheet pinout._



![Alt text](img/Picture9.png?raw=true "IC")




## Electrolytic Capacitors

Not all  [capacitors] are polarized, but when they are, it&#39;s _very_ important not to mix their polarity up.

Ceramic capacitors – the small (1µF and less), commonly yellow guys – are  **not**  polarized. You can stick those in either way.


[_Through-hole_]_ and _ [_SMD_]_ 0.1µF ceramic capacitors. These are NOT polarized._

Electrolytic caps ( [they&#39;ve got electrolytes]), which look like little tin cans,  **are polarized**.

 The negative pin of the cap is usually indicated by a  **&quot;-&quot; marking** , and/or a  **colored strip**  along the can. They might also have a  **longer positive leg**.


![Alt text](img/Picture10.png?raw=true "Capacitors.")



![Alt text](img/Picture11.png?raw=true "Capacitors.")



Applying a negative voltage for an extended period to an electrolytic capacitor results in a catastrophic failure (e.g. they might explode).

### Batteries and Power Supplies

Most batteries will indicate the positive and negative terminals with a &quot;+&quot; or &quot;-&quot; symbol. Other times it might be red wire for positive and a black wire for negative.

![Alt text](img/Picture12.png?raw=true "Batteries.")




### Transistors, MOSFETs, and Voltage Regulators

These (traditionally) three-terminal, polarized components are lumped together because they share similar package types. Through-hole  [transistors], MOSFETs, and voltage regulators commonly come in a TO-92 or TO-220 package, seen below. To find which pin is which, look for the flat edge on the TO-92 package or the metal heatsink on the TO-220, and match that up to the pin-out in the datasheet.



![Alt text](img/Picture13.png?raw=true "transistor")


The dot shows the first pin.



![Alt text](img/Picture14.png?raw=true "end")